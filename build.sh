#!/bin/bash

# try to build the appropriate package for the apparent distribution this is run on

# This program is Free Software released under the GNU GENERAL PUBLIC LICENSE version 3.
# The terms of this license include the rights for anyone to study, modify, and redistribute any form of it.
# You can likely find the full license on your system at: /usr/share/common-licenses/GPL-3

scriptpath="/usr/share/pkgbend"
distro=`"${scriptpath}/distro.sh"`  # attempt to detect package manager

if [[ "$distro" == "debian" ]]; then
  "${scriptpath}/deb.sh" "$@"
elif [ "$distro" = "arch" ]; then
  makepkg PKGBUILD
  # LATER: test this actually works on arch distros
fi
