#!/bin/bash
# test a built debian package - very experimental

failed_string="Test failed. If the package files are correct, try updating ${CONTENTS_PATH}"

# before running this script, export:
# CONTENTS_PATH - path to "pkg_debian" folder or where test results live
# PACKAGE_PATH - path to binary package that will be verified


verify_deb() {
 # expected test result, which should be committed to repository
 previous_deb=`cat ${CONTENTS_PATH}/debcontents`
 # current package just built with "make"
 current_deb=`/usr/share/pkgbend/testdata.sh deb $PACKAGE_PATH`

 echo "Verifying binary package"

 if [[ "$previous_deb" == "$current_deb" ]]; then
  echo "PASS"
 else
  echo "${failed_string}"
 fi
}

verify_sums() {
 echo "Verifying checksums"
 deb_sums=$(sha256sum -c "${CONTENTS_PATH}/SHA256SUMS" --quiet && echo "OK")
 
 if [[ "$deb_sums" == "OK" ]]; then
  echo "PASS"
 else
  echo "${failed_string}"
 fi
}

verify_deb
verify_sums
