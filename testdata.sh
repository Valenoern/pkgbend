#!/bin/bash
# output contents of a package for use in automated testing

deb_data() {  # print contents of debian-style binary (.deb)
 # dpkg --contents is very good for verifying files in a package,
 # but to have a consistent test for the same package the date column must be manually removed.
 
 _digits2="[0-9]\\{2\\}"
 _date="[0-9]\\+-${_digits2}-${_digits2}"
 _time="${_digits2}\:${_digits2}"
 dpkg -c "$package_path" | sed "s/${_date}\s${_time}\s//"
 
 # grep "Checksums-Sha256:" bop-owl-wings_2021.10.23_amd64.buildinfo -A1 | sed -e "s:\\([a-f0-9]\\+\\)\\s[0-9]\\+\\s\\(.\\+\\):\1 \2:"
}

arch_data() {  # print contents of arch-style binary (.tar.zst)
 echo "Arch tests not implemented yet"
 # LATER: try out on arch and implement them
}


scriptpath="/usr/share/pkgbend"
operation="$1"  # subcommand to run - "testdata.sh deb", "testdata.sh arch"/"testdata"
package_path="$2"  # path to built package/archive which will be tested

# LATER:
#  - select test based on package file extension if no test specified
# - add deb-src and/or deb checksum tests

# output test data based on requested package format
if [[ "$operation" == "deb" ]]; then
  deb_data
else
  arch_data
fi
