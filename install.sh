#!/bin/bash

# try to install whatever package which was just built using pkgbend

# This program is Free Software released under the GNU GENERAL PUBLIC LICENSE version 3.
# The terms of this license include the rights for anyone to study, modify, and redistribute any form of it.
# You can likely find the full license on your system at: /usr/share/common-licenses/GPL-3

scriptpath="/usr/share/pkgbend"
pkgname="$2"  # package to install - "pkgbend install packagename"
distro=`"${scriptpath}/distro.sh"`  # attempt to detect package manager


if [[ "$distro" == "debian" ]]; then
  # BUG: this currently requires sudo. see if there's a way to ask for password while running like on arch
  dpkg -i ${pkgname}*.deb
elif [ "$distro" = "arch" ]; then
  pamac install ${pkgname}*.zst
  # LATER: test this actually works on arch distros
fi
