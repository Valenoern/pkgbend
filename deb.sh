#!/bin/bash

# Experimental script to create a debian package from an arch-style PKGBUILD

# This program is Free Software released under the GNU GENERAL PUBLIC LICENSE version 3.
# The terms of this license include the rights for anyone to study, modify, and redistribute any form of it.
# You can likely find the full license on your system at: /usr/share/common-licenses/GPL-3

# This script assumes whoever is building the package has first set the appropriate environment variables,
# in somewhere like ~/.profile
# example values:
#export DEBFULLNAME="Valenoern"
#export DEBEMAIL="valenoern@distributary.network"
#export DEB_SIGN_KEYID="Valenoern <valenoern@distributary.network>"
# LATER: warn if variables aren't set

builddir=`pwd`  # directory the script was called from
pkgbuild="$1"  # path to pkgbuild - "pkgbend ./path/to/PKGBUILD"


# package_source {{{

# BUG: only supports a couple licenses
# LATER: make this script able to extract the git url syntax like makepkg
prepare_variables() {
 source "${builddir}/${pkgbuild}"  # read variables from pkgbuild
 _sourcedir="${pkgname}-${pkgver}"  # directory for package trees
 
 # convert license string to debian one
 # LATER: find GPL version by searching LICENSE
 if [[ "${license}" == "GPL" ]]; then
  license="gpl3"
 elif [[ "${license}" == "MIT" ]]; then
  license="mit"
 elif [[ "${license}" == "BSD" ]]; then
  license="bsd"
 fi
}

clone_source() { # put source code in ${pkgname}-git
 # clone bop source code to "${_sourcedir}" if not present at e.g. "bopwiki-git"
 if [ -d "./${pkgname}-git" ];
  then
    # copy source code folder (following symlinks), because dh_make rejects a symlinked folder
    cp -aL "./${pkgname}-git" "./${_sourcedir}"
  else
    git clone "${_repo}" "./${_sourcedir}"
 fi
}

clean_source() { # create source tarball
 pushd "${_sourcedir}"
 
 # if a commit hash was given
 if [[ ! -z "${_commit}" ]]; then
   # prepare clean source code at version "${pkgver}", without git history
   git reset --hard "${_commit}"
   rm -rf ".git"
 fi
 
 # generate debian directory
 # --i: generate single .deb package, architecture independent
 # --native / --createorig: work with unzipped source tree / create tarball
 # -y: skip y/n prompt
 dh_make --i --native --copyright "$license" -y
 # remove dh_make's example files for legibility; comment out to see them
 rm debian/*.ex debian/*.EX
 popd
}

# BUG: sometimes source directory is recursively duplicated into source again
package_source() { # create debian directory and tarball
 # if source code given, clone source
 # otherwise assume the package repository is the source code
 if [[ ! -z "${_repo}" ]]; then
   clone_source
 else
   echo "No repository given. Using PKGBUILD directory as source"
   cp -r "$builddir" "/tmp/${_sourcedir}"
   mv "/tmp/${_sourcedir}" ./
 fi
 
 clean_source
 
 # generate .dsc metadata and tarball
 # LATER: is "debian" supposed to appear in a tarball? I don't know
 dpkg-source -b "${_sourcedir}"
}

# }}}

# package binary {{{

# TODO: move cleanup script into package's directory
clean_binary() {
 # prepare folders for creating package
 mv "./${_sourcedir}" "./${_sourcedir}-src"
 mkdir "./${_sourcedir}"
 #pushd "${_sourcedir}-src"
 # for now, remove files in the source which won't be used
 #rm -r LICENSE test-bats.bats tests.bats tests.sh
 #popd
}

# BUG: files aren't put in package or something
run_pkgbuild() { # run PKGBUILD at top of directory
 # set variables to things a PKGBUILD expects
 ln -s "./${_sourcedir}-src" "./${pkgname}"  # duct-tape source directory to git name
 export srcdir=`pwd`
 export pkgdir="${srcdir}/${_sourcedir}"
 
 # attempt to run "package" function within PKGBUILD
 package
 
 rm "./${pkgname}"  # clean up duct tape link
}

# TODO: copy files only if they exist
fix_debian() { # use any existing "debian/" files
 pushd "${_sourcedir}"
 cp -a "../${_sourcedir}-src/debian" ./
 cp ../pkg_debian/install ../pkg_debian/control ../pkg_debian/compat debian/
 
 # install: this file is required to copy "usr" etc into the deb
 # control: provides the package metadata such as dependencies
 popd
}

# BUG: no sign
# LATER: just create a no-sign switch for this script
build_binary() { # try to build .deb
 pushd "${_sourcedir}"
 dpkg-buildpackage --build=binary --no-sign
 # --no-sign can be used to skip signing for testing
 popd
 
 # clean up extra "src" directory
 rm -rf "./${_sourcedir}-src"
}

package_binary() {
 clean_binary  # create "./${_sourcedir}-src"
 run_pkgbuild
 
 fix_debian
 build_binary
}

# }}}


main() {
 cd "$builddir"
 prepare_variables
 
 echo "Creating debian package in ${_sourcedir}"
 
 package_source
 package_binary
}


main
