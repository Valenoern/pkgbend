pkgbend is a tool to simplify packaging.

The first few times I tried to make linux packages, which were debian style, I was surprised by how complicated it was.  
Later, as I discovered arch-based distros and the Arch User Repository, I thought, everyone should have the opportunity to create a PKGBUILD and then build it on other distributions. Things would be much simpler, and there might even be less of a role for Docker, etc.  
So after a while I created pkgbend.


## Using pkgbend

For the best introduction to pkgbend, you should take a look at `Makefile.example`.  
Essentially, you use this Makefile to build all your packages:

```
pkgname = example-package

package: clean
	pkgbend PKGBUILD

install:
	pkgbend install $(pkgname)
```

An alpha/beta tester who wants to test your software on a new architecture, etc, can simply download the PKGBUILD repository for your package and run
```
make && make install
# NOTE: you currently have to run "sudo make install" on debian
```

That's it. Testers don't have to rummage through repositories to find out what script builds the software, and the program is installed in the package manager, able to be removed the standard way.


## Status

Currently this tool is extremely experimental and needs to be tested with a lot more packages.  
You should not necessarily expect it to work correctly.

It also only works for arch- and debian-style packages, though more styles of package may be added later.


## License

pkgbend is released under the GPL3.

By the time it's finished it might switch to the AGPL3, due to the influence of big centralised package platforms that build and sign packages server-side.  
pkgbend might well be running at your distribution's central package servers one day, who can really know?


<!-- eof -->
