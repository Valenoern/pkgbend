#!/bin/bash

# main "table of contents" script for pkgbend

# dependencies: coreutils git
# optdepends: make  dh-make   pacman | apt
# NOTE: the gpl-3 file is installed by base-files on debian

# packaging notes:
# sudo ln -s [...]/pkgbend /usr/share/pkgbend

# !/bin/bash
#/usr/share/pkgbend/pkgbend.sh "$@"


scriptpath="/usr/share/pkgbend"
operation="$1"  # subcommand to run - "pkgbend", "pkgbend install"

# when running "pkgbend install"
if [[ "$operation" == "install" ]]; then
  "${scriptpath}/install.sh" "$@"
# when running "pkgbend"
else
  "${scriptpath}/build.sh" "$@"
fi

# this is structured assuming you will use a Makefile like "Makefile.example" distributed with pkgbend
